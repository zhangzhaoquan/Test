package com.qianfeng.liwushuo.Constant;

/**
 * Created by Lenovo on 2015/8/13 0013.
 */
public class URLConstant {

    public static final String CHOICE_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/100/items?limit=20&offset=0&gender=1&generation=1";
    public static final String GIFT_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/111/items?limit=20&offset=0&gender=1&generation=1";
    public static final String FOOD_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/118/items?limit=20&offset=0&gender=1&generation=1";
    public static final String DIGITAL_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/121/items?limit=20&offset=0&gender=1&generation=1";
    public static final String SPORTS_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/123/items?limit=20&offset=0&gender=1&generation=1";
    public static final String ENTERTAINMENT_LISTVIEW_URL = "http://api.liwushuo.com/v2/channels/120/items?limit=20&offset=0&gender=1&generation=1";

    public static final String CHOICE_BANNER_URL = "http://api.liwushuo.com/v2/banners";
    public static final String SKU_URL = "http://api.liwushuo.com/v2/items?limit=20&offset=0&gender=1&generation=1";
    public static final String STRATEGY_TITLE_URL = "http://api.liwushuo.com/v2/collections?limit=10&offset=0";
    public static final String STRATEGY_LISTVIEW_URL = "http://api.liwushuo.com/v2/channel_groups/all";
    public static final String GIFT_URL = "http://api.liwushuo.com/v2/item_categories/tree";

}
